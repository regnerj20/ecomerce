$(document).ready(function(){
    $('#buscar, .fa-search').mouseenter(function(){
        $('.logo').hide()
    })

    $('#buscar, .fa-search').mouseleave(function(){
        $('.logo').show()
    })

    $('.fa-bars').click(function(){
        $('.navbar').toggle()
        $(this).toggleClass('fa-times')
    })

    $('.navbar, .navbar a').click(function(){
        $('.navbar').hide()
        $('.fa-bars').removeClass('fa-times')
    })   

    $(window).on('scroll load', function(){
        if($(window).scrollTop() > 20){/*cuantos pixles debo de scrollear */
            $('#header').css({
                'background' : '#EB4D4B',
                'box-shadow' : '0 .1rem .3rem #000'
            })
        }else{
            $('#header').css({
                'background' : '#333',
                'box-shadow' : 'none'
            })
        }
    })

    $('.inicio-slider').owlCarousel({
        loop:true,
        margin:40 ,
        nav:true,
        items:1,
        autoplay:true
    })

    $('.producto-slider').owlCarousel({
        loop:true,
        nav:true,
        items:3,
        autoplay:true,
        center:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })

    $('.comentario-slider').owlCarousel({
        loop:true,
        nav:true,
        items:1,
        autoplay:true
    })

    $('.marca-slider').owlCarousel({
        loop:true,
        items:4,
        nav:false,
        dots:false,
        autoplay:true,
        responsive:{
            0:{
                items:1
            },
            400:{
                items:2
            },
            550:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
})